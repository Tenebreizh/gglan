<?php 

function afficheEquipe()
{
	$sql = "SELECT * FROM equipe;";
	$results = tableSQL($sql);

	foreach ($results as $ligne)
	{
		//Si l'equipe a 5 joueurs
		if ($ligne[2] == 5)
		{
			equipeFull($ligne[0],$ligne[1]);
		}
		elseif ($ligne[2] == 0)
		{
			$equipe = "no";
			exit();
		}
		else
		{
			equipeNoFull($ligne[0],$ligne[1],$ligne[2]);
		}
	}
	if ($equipe == "no"){
		noEquipe();
	}
}

function afficheJoueur()
{
	//CS:GO, Starcraft II Legacy Of The Void
	$jeu = "CS:GO";

	$sql = "SELECT * FROM joueur WHERE jeu != '$jeu';";
	$joueurs = tableSQL($sql);


	echo"<div class='col-lg-12'>";
		echo"<table class='table table-bordered table-striped table-condensed'>";
		    echo "<caption>";
		        echo "<h3>Joueurs enregistrées pour la prochaine LAN</h3>";
		    echo "</caption>";

		    echo "<thead>";
		        echo "<th>Joueurs</th>";
		        echo "<th>Jeu</th>";
		        echo "<th>Niveau</th>";
		    echo "</thead>";
		    echo "<tbody>";
				foreach ($joueurs as $joueur)
				{
					echo"<tr>";
						echo"<td>$joueur[0]</td>";
						echo"<td>$joueur[2]</td>";
						echo"<td>$joueur[3]</td>";
					echo"</tr>";
				}	
		    echo "</tbody>";
		echo "</table>";
	echo "</div>";
}

function afficheListeBoisson()
{
	$sql = "SELECT id, boisson FROM commande ORDER BY boisson;";
	$boissons = tableSQL($sql);


	echo "<select class='form-control' name='boisson' id='commande'>";
		echo"<option selected disabled>-- Choisir une boisson --</option>";                                                   
		foreach ($boissons as $ligne)
		{
			echo "<option value='$ligne[0]'>$ligne[1]</option>";
		}
	echo "</select>";
}

function afficheListeEquipe()
{
	$sql = "SELECT nom, joueurs FROM equipe WHERE joueurs != 5 ORDER BY nom;";
	$equipes = tableSQL($sql);

	echo "<select class='form-control' name='equipe' id='equipe'>";
        echo "<option selected disabled>-- Choisir votre equipe --</option>";
		foreach ($equipes as $ligne)
		{
			echo "<option value='$ligne[0]'><strong>$ligne[0]</strong> $ligne[1] / 5</option>";
		}
		echo "<option value='creationTeam'>J'inscris la mienne</option>";
		echo "<option value='seul'>Aucune</option>";
	echo "</select>";
}

function afficheListeJeux()
{
	$sql = "SELECT jeu FROM jeux ORDER BY jeu;";
	$jeux = tableSQL($sql);


	echo "<select class='form-control' name='jeu' id='chooseJeu'>";
		echo"<option selected disabled>-- Jeu --</option>";                                                   
		foreach ($jeux as $ligne)
		{
			//Desactive la selection des jeux selectionné
			if ($ligne[0] == "CS:GO")
			{
				echo "<option value='$ligne[0]' disabled>$ligne[0]</option>";
			}
			else
			{
				echo "<option value='$ligne[0]'>$ligne[0]</option>";
			}
			
		}
	echo "</select>";
}

function afficheNiveau($jeu)
{
	if ($jeu == "Starcraft II Legacy Of The Void") 
	{
		$sql = "SELECT niveau FROM niveau WHERE jeu= '$jeu' ORDER BY ordre";
		$niveaux = tableSQL($sql);

		echo "<select class='form-control' name='niveau'>";
			echo "<option selected disabled>-- Votre niveau --</option>";
			foreach ($niveaux as $niveau) 
			{
				echo "<option value='$niveau[0]'>$niveau[0]</option>";
			}
		echo "</select>";
	}



	elseif ($jeu = "CS:GO") 
	{
		//Les ranks CS:GO
	}
}

function affichePhotos()
{	
	$sql = "SELECT * FROM images";
	$photos = tableSQL($sql);

	foreach ($photos as $photo)
	{
		$chemin = $photo[1];
		echo"<div class='col-xs-4 col-sm-3 col-md-2'>";
			echo "<img src='$chemin' class='img-responsive img-rounded' />";
		echo"</div>";
	}
}

function afficheReglement()
{
	$sql = "SELECT * FROM reglement;";
	$articles = tableSQL($sql);

	$cpt = 1;
	foreach ($articles as $article)
	{
		echo "<br />";
		echo "<h3 class='text-uppercase red'>Article $cpt </h3>";
		echo "<h4>$article[1]</h4>";
		$cpt++;
	}
}









// ***** EQUIPE *****
//Affiche la ligne d'une equipe complète avec son capitaine et ses joueurs
function equipeFull($equipe, $capitaine)
{
	echo"<div class='col-lg-12'>";
        echo"<table class='table table-bordered table-striped table-condensed'>";
            echo "<caption>";
                echo "<h3>Teams complète</h3>";
            echo "</caption>";

            echo "<thead>";
                echo "<th>Nom</th>";
                echo "<th>Joueurs</th>";
            echo "</thead>";
            echo "<tbody>";
            		//Recupère le nom des joueurs de l'équipe sauf celui du capitaine
					$sqlJoueurs = "SELECT pseudo FROM joueur WHERE equipe='$equipe' AND pseudo != '$capitaine';";
					$joueurs = tableSQL($sqlJoueurs);
					echo"<tr>";
						echo"<td>$equipe</td>";
						//On ecrit le nom du capitaine en gras avec les autres joueurs de son équipe
						echo"<td> <strong>$capitaine</strong>";
								foreach ($joueurs as $joueur)
								{
								    echo "<span class='playerList'>$joueur[0]</span>";
								}
						echo "</td>";
					echo"</tr>";
            echo "</tbody>";
        echo "</table>";
    echo "</div>";
}

//Affiche la ligne d'une equipe incomplète avec son capitaine et ses joueurs
function equipeNoFull($equipe, $capitaine, $nombreJoueurs)
{
	echo"<div class='col-lg-12'>";
        echo"<table class='table table-bordered table-striped table-condensed'>";
            echo "<caption>";
                echo "<h3>Teams incomplète</h3>";
            echo "</caption>";

            echo "<thead>";
                echo "<th>Nom</th>";
                echo "<th>Joueurs</th>";
                echo "<th>Nombre</th>";
            echo "</thead>";
            echo "<tbody>";
            		//Recupère le nom des joueurs de l'équipe sauf celui du capitaine
					$sqlJoueurs = "SELECT pseudo FROM joueur WHERE equipe='$equipe' AND pseudo != '$capitaine';";
					$joueurs = tableSQL($sqlJoueurs);
					echo"<tr>";
						echo"<td>$equipe</td>";
						//On ecrit le nom du capitaine en gras avec les autres joueurs de son équipe
						echo"<td> <strong>$capitaine</strong>";
							foreach ($joueurs as $joueur)
							{
								   echo "<span class='playerList'>$joueur[0]</span>";
							}
						echo "</td>";
						echo"<td>$nombreJoueurs <strong> / 5</strong></td>";
					echo"</tr>";
            echo "</tbody>";
        echo "</table>";
    echo "</div>";
}

function noEquipe()
{
	echo"<div class='col-lg-12'>";
	    echo "<h1>Aucune team inscrite pour le moment...</h1>";
    echo "</div>";
}
// ******************

?>