<?php 
$sql = "SELECT * FROM equipe ;";
$results = tableSQL($sql);

foreach ($results as $ligne)
{
	//Si l'equipe a 5 joueurs
	if ($ligne[2] == 5)
	{
		equipeFull($ligne[0],$ligne[1]);
	}
	elseif ($ligne[2] == 0)
	{
		noEquipe();
	}
	else
	{
		equipeNoFull($ligne[0],$ligne[1],$ligne[2]);
	}
}

?>
<?php 

//Affiche la ligne d'une equipe complète avec son capitaine et ses joueurs
function equipeFull($equipe, $capitaine)
{
	echo"<div class='col-lg-12'>";
        echo"<table class='table table-bordered table-striped table-condensed'>";
            echo "<caption>";
                echo "<h3>Teams complète</h3>";
            echo "</caption>";

            echo "<thead>";
                echo "<th>Nom</th>";
                echo "<th>Joueurs</th>";
            echo "</thead>";
            echo "<tbody>";
            		//Recupère le nom des joueurs de l'équipe sauf celui du capitaine
					$sqlJoueurs = "SELECT pseudo FROM joueur WHERE equipe='$equipe' AND pseudo != '$capitaine';";
					$joueurs = tableSQL($sqlJoueurs);
					echo"<tr>";
						echo"<td>$equipe</td>";
						//On ecrit le nom du capitaine en gras avec les autres joueurs de son équipe
						echo"<td> <strong>$capitaine</strong>";
								foreach ($joueurs as $joueur)
								{
								    echo "<span class='playerList'>$joueur[0]</span>";
								}
						echo "</td>";
					echo"</tr>";
            echo "</tbody>";
        echo "</table>";
    echo "</div>";
}


//Affiche la ligne d'une equipe incomplète avec son capitaine et ses joueurs
function equipeNoFull($equipe, $capitaine, $nombreJoueurs)
{
	echo"<div class='col-lg-12'>";
        echo"<table class='table table-bordered table-striped table-condensed'>";
            echo "<caption>";
                echo "<h3>Teams incomplète</h3>";
            echo "</caption>";

            echo "<thead>";
                echo "<th>Nom</th>";
                echo "<th>Joueurs</th>";
                echo "<th>Nombre</th>";
            echo "</thead>";
            echo "<tbody>";
            		//Recupère le nom des joueurs de l'équipe sauf celui du capitaine
					$sqlJoueurs = "SELECT pseudo FROM joueur WHERE equipe='$equipe' AND pseudo != '$capitaine';";
					$joueurs = tableSQL($sqlJoueurs);
					echo"<tr>";
						echo"<td>$equipe</td>";
						//On ecrit le nom du capitaine en gras avec les autres joueurs de son équipe
						echo"<td> <strong>$capitaine</strong>";
							foreach ($joueurs as $joueur)
							{
								   echo "<span class='playerList'>$joueur[0]</span>";
							}
						echo "</td>";
						echo"<td>$nombreJoueurs <strong> / 5</strong></td>";
					echo"</tr>";
            echo "</tbody>";
        echo "</table>";
    echo "</div>";
}

function noEquipe()
{
	echo"<div class='col-lg-12'>";
	    echo "<h1>Aucune team inscrite pour le moment...</h1>";
    echo "</div>";
}


?>