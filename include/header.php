<nav class="navbar navbar-inverse navbar-fixed-top">
    <header class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">GG-LAN</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="/vues/reglement.php">Règlement</a></li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" type="button" data-toggle="dropdown">Inscription <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                        <li><a href="/vues/inscriptionJoueur.php">Inscription Joueur</a></li>
                        <!-- <li><a href="/vues/inscriptionEquipe.php">Inscription Equipe</a></li> -->
                      </ul>
                </li>

                <li><a href="/vues/teams.php">Joueur</a></li>
                <li><a href="/vues/info.php">Où ça</a></li>
                <li><a href="/vues/materiel.php">Matériel</a></li>
                <!-- <li><a href="/vues/photos.php">Photos</a></li> -->
            </ul>
        </div><!--/.nav-collapse -->
    </header>
</nav>