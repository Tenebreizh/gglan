<!DOCTYPE html>
<html>
<?php include "../include/head.php";?>
<body>
    <?php include "../include/header.php" ?>

    <div class="container container1">
        <div class="row">
            <div class="col-lg-12">
                <form class="form-horizontal" method="POST" action="/controleurs/enregistre.php?joueur">
                    <legend>
                        Inscription Joueur
                        <blockquote>Si vous avez <strong><u>moins de 18 ans</u></strong>, vous devrez vous munir d’une autorisation parentale à présenter le jour de la LAN (vous la trouverez <a href="https://mega.nz/#!Jwpm3IxQ!mJDvhg6EN0kR9ztu_bWYvGyS6v0RdV6GFkt8n9WnxH0">ici</a> )</blockquote>
                    </legend>

                    <!-- Nom -->
                    <div class="form-group">
                        <label for="nom" class="col-sm-2 control-label">Nom: </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom" required>
                            </div>
                    </div>

                    <!-- Prenom -->
                    <div class="form-group">
                        <label for="prenom" class="col-sm-2 control-label">Prenom: </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="prenom" id="prenom" placeholder="Prenom" required>
                            </div>
                    </div>

                    <!-- Age -->
                    <div class="form-group">
                        <label for="age" class="col-sm-2 control-label">Age: </label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="age" id="age" min="12" max="99" placeholder="Age" required>
                            </div>
                    </div>

                    <!-- Email -->
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email: </label>
                            <div class="col-sm-10">
                                <input type="mail" class="form-control" name="email" id="email" placeholder="Email" required>
                            </div>
                    </div>

                    <!-- Pseudo -->
                    <div class="form-group">
                        <label for="pseudo" class="col-sm-2 control-label">Pseudo: </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="pseudo" id="pseudo" placeholder="Pseudo" required>
                            </div>
                    </div>

                    <!-- Jeu -->
                    <div class="form-group">
                        <label for="jeu" class="col-sm-2 control-label">Jeu: </label>
                            <div class="col-sm-10">
                                <?php afficheListeJeux(); ?>
                            </div>
                    </div>

                    <div id="target">
                    </div>

                    <!-- Boisson -->
                    <div class="form-group">
                        <label for="commande" class="col-sm-2 control-label"><abbr title="A titre indicatif, choisir une boisson ne vous engage à rien">Boisson </abbr>:</label>
                            <div class="col-sm-10">
                                <?php afficheListeBoisson(); ?>
                            </div>
                    </div>

                    <!-- Bouton d'inscription -->
                    <div class="form-group">
                        <div class="col-sm-6">
                            <input type="hidden" name="joueur">
                            <input type="submit" class="btn btn-success" value="S'inscrire">
                            &nbsp <input type="checkbox" required>J'accepte <a href="/vues/reglement.php" target="_blank">les règles</a> de la GG-LAN</input>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <br />
    </div>


    <?php
        include "../include/footer.php";
        deconnexion();

        if(isset($_GET['ok']))
        {
            echo "<script>alert('Vous avez bien été inscrit !')</script>";
        }

        if(isset($_GET['existe']))
        {
            echo "<script>alert('Ce joueur est deja inscrit')</script>";
        }
    ?>
</body>
</html>
