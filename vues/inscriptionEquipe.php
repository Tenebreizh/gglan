<!DOCTYPE html>
<html>
<?php include "../include/head.php" ?>
<body>
    <?php include "../include/header.php" ?> 

    <div class="container container1">
        <div class="row">
            <div class="col-lg-12">
                <form class="form-horizontal" method="GET" action="" >
                    <legend>
                        Inscription Equipe
                        <blockquote>Pour inscrire votre équipe vous devez être au minimum <strong><u>2 Joueurs</u></strong></blockquote>
                    </legend>
                    <div class="form-group">
                        <label for="nom" class="col-sm-2 control-label">Nom: </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="nom" placeholder="Nom" required autofocus>
                            </div>
                    </div>
                    <div class="form-group">
                        <label for="capitaine" class="col-sm-2 control-label">Capitaine: </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="capitaine" placeholder="Capitaine" required>
                            </div>
                    </div>
                    <div class="form-group">
                        <label for="joueur1" class="col-sm-2 control-label">Joueur 1: </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="joueur1" placeholder="Pseudo..." required>
                            </div>
                    </div>
                    <div class="form-group">
                        <label for="joueur2" class="col-sm-2 control-label">Joueur 2: </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="joueur2" placeholder="Pseudo..."  required>
                            </div>
                    </div>
                    <div class="form-group" id="add">
                            <div class="col-sm-1">
                                <button type="button" class="btn btn-primary" id="addBtn" >
                                    <span class="glyphicon glyphicon-plus"></span>Joueur
                                </button>
                            </div>
                    </div>

                    <!-- Bouton d'inscription -->
                    <div class="form-group">
                        <div class="col-sm-6">
                            <input type="hidden" name="equipe">
                            <input type="submit" class="btn btn-success" value="S'inscrire">
                        </div>
                    </div> 

                </form>
            </div>
        </div>
        <br /> 
    </div>


    <?php 
        include "../include/footer.php"; 
        deconnexion();
    ?>
</body>
</html>