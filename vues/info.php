<!DOCTYPE html>
<html>
<?php include "../include/head.php" ?>
<body>
    <?php include "../include/header.php" ?> 

    <div class="container container1">
        <div class="row">
            <div class="col-lg-12">
                <legend>
                    Où ça ?
                    <blockquote>La LAN aura lieu au PL Guérin, plus d’informations ci-dessous</blockquote>
                </legend>

                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10596.233437730622!2d-4.4841554!3d48.3978224!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7a70424930d3d71e!2sPatronage+La%C3%AFque+Gu%C3%A9rin!5e0!3m2!1sfr!2sfr!4v1495558398028" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>

                <address>
                    <h4>1 rue Alexandre Ribot, 29200 BREST</h4>
                    <h4>Patronage Laïque Guérin</h4>
                </address>                
            </div>
        </div>
    </div>


    <?php include "../include/footer.php" ?>
</body>
</html>