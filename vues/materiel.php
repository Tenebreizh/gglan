<!DOCTYPE html>
<html>
<?php include "../include/head.php" ?>
<body>
    <?php include "../include/header.php" ?> 

    <div class="container container1">
        <div class="row">
            <div class="col-lg-12">
                <legend>
                    Materiel
                    <blockquote>Voici une liste du matériel que chaque joueur devra <u><strong>au minimum</strong></u> prendre avec lui pour la LAN</blockquote>
                </legend>

                <h4 class="text-left">Counter-Strike:Global Offensive :</h4>
                
                <ul class="text-left">
                    <li>
                        <h4><u>Votre matériel informatique</u></h4>
                        <ul>
                            <li>PC (Fixe ou portable)</li>
                            <li>Ecran</li>
                            <li>Clavier</li>
                            <li>Souris</li>
                            <li>Tapis de souris</li>
                            <li>Casque</li>
                        </ul>
                    </li>
                    <li>
                        <h4><u>Un câble ethernet</u></h4> Assez long pour pouvoir vous connecter au réseau local
                    </li>
                    <li>
                        <h4><u>Une multiprise</u></h4>
                    </li>
                    <li>
                        <h4><u>Votre carte d'identité avec l'autorisation parentale pour les -18ans, </u></h4> 
                        <a href="/ressource/AutorisationParentaleGGLAN.pdf" download="AutorisationParentaleGGLAN.pdf">Telechargez l'autorisation ici</a>
                    </li>
                    <li>
                        <h4><u>De l'argent</u></h4> Pour pouvoir payer votre entrée, <strong><u>les boissons et les repas sont compris</u></strong>, cependant vous pouvez apporter votre propre repas/boissons ou argent en plus pour aller manger en ville lors des pauses aménagées pour.
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <?php include "../include/footer.php" ?>
</body>
</html>