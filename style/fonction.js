var cpt = 2
$("#addBtn").click(function()
{
	var addBtn = document.getElementById("addBtn")
	cpt += 1

	if (cpt <= 5)
	{
		addBtn.className = "btn btn-primary active"
		//Creation element 
		var div = "<div class='form-group'>"

		var label = "<label for='joueur"+cpt+"' class='col-sm-2 control-label'>Joueur "+cpt+": </label>"

		var divLigne = "<div class='col-sm-10'>"

		var input = "<input type='text' class='form-control' id='joueur"+cpt+"' placeholder='Pseudo...'  required>"

		var divClose = "</div>"
		// ************************* 

		//On ajoute le champ à la page HTML
		var add = div + label + divLigne + input + divClose + divClose
		$("#add").before(add)
	}
	
	if (cpt == 5 || cpt > 5)
	{
		addBtn.className = "btn btn-primary disabled"
	}
})

$("#chooseJeu").change(function()
{
	var jeu = $("#chooseJeu").val()
	console.log("Le jeu choisi est: " + jeu)

	if (jeu === "Starcraft II Legacy Of The Void")
	{
		$("#target").load("/include/form/formInscriptionStarcraft.php")
	}
	else if (jeu === "CS:GO")
	{
		$("#target").load("/include/form/formInscriptionCSGO.php")
	} 
	
})