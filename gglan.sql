-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mer 14 Juin 2017 à 17:03
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gglanv1`
--

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id` tinyint(2) NOT NULL,
  `boisson` varchar(20) NOT NULL,
  `prix` double(5,2) DEFAULT NULL,
  `quantite` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `commande`
--

INSERT INTO `commande` (`id`, `boisson`, `prix`, `quantite`) VALUES
(1, 'Coca-Cola', 1.37, 1),
(2, 'Eau', 0.72, 1),
(3, 'Jus d\'orange', 1.00, 1),
(4, 'Orangina', 1.00, 0);

-- --------------------------------------------------------

--
-- Structure de la table `equipe`
--

CREATE TABLE `equipe` (
  `nom` varchar(255) NOT NULL,
  `capitaine` varchar(255) NOT NULL,
  `joueurs` tinyint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `equipe`
--

INSERT INTO `equipe` (`nom`, `capitaine`, `joueurs`) VALUES
('Testeur', 'test1', 5),
('ZBEUBZBEUB', 'Marshmaloo', 2);

-- --------------------------------------------------------

--
-- Structure de la table `images`
--

CREATE TABLE `images` (
  `id` tinyint(2) NOT NULL,
  `link` text NOT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `gglanNumero` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `images`
--

INSERT INTO `images` (`id`, `link`, `nom`, `gglanNumero`) VALUES
(2, '/images/site/banniere.png', 'banniere', NULL),
(3, '/images/photosLAN/photo1.png', 'photo1', 4),
(6, '/images/photosLAN/photo2.png', 'photo2', 4);

-- --------------------------------------------------------

--
-- Structure de la table `jeux`
--

CREATE TABLE `jeux` (
  `jeu` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jeux`
--

INSERT INTO `jeux` (`jeu`) VALUES
('CS:GO'),
('Fifa 17'),
('Starcraft II Legacy Of The Void');

-- --------------------------------------------------------

--
-- Structure de la table `joueur`
--

CREATE TABLE `joueur` (
  `pseudo` varchar(255) NOT NULL,
  `equipe` varchar(255) DEFAULT NULL,
  `jeu` varchar(100) DEFAULT NULL,
  `niveau` varchar(100) DEFAULT NULL,
  `mail` varchar(255) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `age` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `joueur`
--

INSERT INTO `joueur` (`pseudo`, `equipe`, `jeu`, `niveau`, `mail`, `nom`, `prenom`, `age`) VALUES
('Banboula', NULL, 'CS:GO', NULL, 'thibaud.philippi97@gmail.com ', 'Jean', 'Jacque', 22),
('earezrzererzerezrezfsdfd', NULL, 'Starcraft II Legacy Of The Void', 'Diamant1', 'eireuibr@iuiez.com', 'roejrio', 'rgfjpÃ¹r!mgkn', 15),
('leGrandKebab', 'ZBEUBZBEUB', 'CS:GO', NULL, 'thibaud29@protonmail.com', 'bat', 'man', 0),
('Marshmaloo', 'ZBEUBZBEUB', 'CS:GO', NULL, 'jpp@gmail.com', 'Doe', 'John', 0),
('Tenebreizh', NULL, 'Starcraft II Legacy Of The Void', 'Grand maitre', 'thibaud97@hotmail.fr', 'Philippi', 'Thibaud', 20),
('test1', 'Testeur', 'CS:GO', NULL, 'test1@gmail.com', 'test1', 'test1', 0),
('test2', 'Testeur', 'CS:GO', NULL, 'test2@gmail.com', 'test2', 'test2', 0),
('test3', 'Testeur', 'CS:GO', NULL, 'test3@gmail.com', 'test3', 'test3', 0),
('test4', 'Testeur', 'CS:GO', NULL, 'test4@gmail.com', 'test4', 'test4', 0),
('test5', 'Testeur', 'CS:GO', NULL, 'test5@gmail.com', 'test5', 'test5', 0);

-- --------------------------------------------------------

--
-- Structure de la table `niveau`
--

CREATE TABLE `niveau` (
  `ordre` tinyint(3) NOT NULL,
  `jeu` varchar(100) NOT NULL,
  `niveau` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `niveau`
--

INSERT INTO `niveau` (`ordre`, `jeu`, `niveau`) VALUES
(4, 'Starcraft II Legacy Of The Void', 'Argent1'),
(5, 'Starcraft II Legacy Of The Void', 'Argent2'),
(6, 'Starcraft II Legacy Of The Void', 'Argent3'),
(3, 'Starcraft II Legacy Of The Void', 'Bronze1'),
(2, 'Starcraft II Legacy Of The Void', 'Bronze2'),
(1, 'Starcraft II Legacy Of The Void', 'Bronze3'),
(15, 'Starcraft II Legacy Of The Void', 'Diamant1'),
(14, 'Starcraft II Legacy Of The Void', 'Diamant2'),
(13, 'Starcraft II Legacy Of The Void', 'Diamant3'),
(19, 'Starcraft II Legacy Of The Void', 'Grand maitre'),
(18, 'Starcraft II Legacy Of The Void', 'Maitre1'),
(17, 'Starcraft II Legacy Of The Void', 'Maitre2'),
(16, 'Starcraft II Legacy Of The Void', 'Maitre3'),
(9, 'Starcraft II Legacy Of The Void', 'Or1'),
(8, 'Starcraft II Legacy Of The Void', 'Or2'),
(7, 'Starcraft II Legacy Of The Void', 'Or3'),
(12, 'Starcraft II Legacy Of The Void', 'Platine1'),
(11, 'Starcraft II Legacy Of The Void', 'Platine2'),
(10, 'Starcraft II Legacy Of The Void', 'Platine3');

-- --------------------------------------------------------

--
-- Structure de la table `reglement`
--

CREATE TABLE `reglement` (
  `id` tinyint(2) NOT NULL,
  `article` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `reglement`
--

INSERT INTO `reglement` (`id`, `article`) VALUES
(1, 'L’utilisation de tout système audio (enceintes, caisson de basses, etc …) est interdite, seuls les casques d’écoute et les écouteurs sont autorisés.'),
(2, 'Toute personne désireuse de s’inscrire à la GGLAN doit formellement indiquer lors de son inscription si elle est sujette aux crises d’épilepsie, ou pertes de connaissance due à la stimulation visuelle de certains effets stroboscopiques ou lumineux. De plus, elle certifie que les données entrées sur le site lors de l’inscription sont correctes et authentiques.\r\n\r\n'),
(3, 'Les organisateurs ne pourront en aucun cas être tenus responsables quant aux agissements des participants en dehors de l’enceinte de la manifestation.'),
(4, 'En cas de dégradation, de casse ou de vol du matériel des participants, l’organisation ne pourra être tenue responsable. Toutes dégradations de matériels causées à un tiers devront être réparées financièrement par l’auteur des faits.'),
(5, 'En aucun cas, l’organisation ne pourra être tenue pour responsable des dommages, pertes, ou vols, qui concerneraient les biens apportés par les participants, ainsi que d’éventuels dommages qui pourraient être occasionnés aux autres participants; ainsi en cas de contrôle anti-piratage..'),
(6, 'L’organisation se réserve le droit de se retourner contre un joueur ayant causé des dégradations au bâtiment ou au mobilier lors de l’événement.'),
(7, 'Les animaux ne sont pas autorisés.'),
(8, 'Les bouteilles en verre sont interdites dans le bâtiment, toute bouteille trouvée sera confisquée et détruite. L’organisation se garde aussi le droit d’exclure le joueur de la manifestation.'),
(9, 'Il est strictement interdit d’utiliser des appareils électriques autres que votre PC (onduleur, ventilateur,…)'),
(10, 'Toute manipulation électrique ou sur le réseau est interdite, en cas de problème demander l’aide d’un organisateur.'),
(11, 'Il est interdit d’avoir des propos racistes et injurieux envers les autres participants et les organisateurs.'),
(12, 'Les mineurs seront acceptés sous la responsabilité de leurs parents'),
(13, 'Toute tentative de saturation du réseau, de piratage, de vol ainsi que tout comportement déplacé entraînera l’expulsion de la ou les personnes concernées.'),
(14, 'En participant, vous vous engagez à ne pas participer au piratage informatique sous toutes ses formes. Vous êtes responsable du contenu numérique de vos disques et de votre matériel.'),
(15, 'L’association organisatrice se réserve le droit de modifier, ou d’annuler purement et simplement la lan, en raison de tout évènement indépendant de sa volonté, ainsi que de modifier le montant des différents prize pool, au prorata du nombre de joueurs.'),
(16, 'La vente ou l’échange de logiciels, d’œuvres musicales, protégées sont interdits.'),
(17, 'Chaque joueur doit posséder les licences de tous les jeux et logiciels ainsi que du système d’exploitation installés sur le pc. Nous vous rappelons que le piratage est un acte répréhensible par la loi.'),
(18, 'Il est strictement interdit de fumer dans la salle et de consommer toute sorte de drogue. La consommation d’alcool est formellement interdite. Toute personne en état d’ébriété, ou sous l’action de produits stupéfiants, qui perturbe le bon déroulement de la manifestation sera immédiatement expulsée de la lan.'),
(19, 'Cette LAN est de type BYOC : bring your own computer. Cela signifie que chaque joueur participant doit rapporter l’intégralité de son matériel de jeu. Si ce n’est pas le cas, les organisateurs se réservent le droit d’annuler la participation aux tournois.'),
(20, 'Les places s’achètent en ligne, au local et sur place si pas de possibilité de s’inscrire autrement. Une fois achetées, elles ne sont ni échangeables, ni remboursables. Les places visiteurs achetées sur place ne sont ni échangeables, ni remboursables.'),
(21, 'Les organisateurs se réservent le droit d’expulser toutes personnes en cas de téléchargement abusif (sur-utilisation de la connexion, usage autre que la connections nécessaire au jeu lors sur le temps du tournoi).'),
(22, 'Les organisateurs se réservent le droit d’expulser toutes personnes ne respectant pas ce règlement.');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`nom`),
  ADD KEY `capitaine` (`capitaine`);

--
-- Index pour la table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `jeux`
--
ALTER TABLE `jeux`
  ADD PRIMARY KEY (`jeu`);

--
-- Index pour la table `joueur`
--
ALTER TABLE `joueur`
  ADD PRIMARY KEY (`pseudo`),
  ADD KEY `equipe` (`equipe`),
  ADD KEY `jeu` (`jeu`),
  ADD KEY `niveau` (`niveau`);

--
-- Index pour la table `niveau`
--
ALTER TABLE `niveau`
  ADD PRIMARY KEY (`niveau`),
  ADD KEY `jeu` (`jeu`);

--
-- Index pour la table `reglement`
--
ALTER TABLE `reglement`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `images`
--
ALTER TABLE `images`
  MODIFY `id` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `reglement`
--
ALTER TABLE `reglement`
  MODIFY `id` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `joueur`
--
ALTER TABLE `joueur`
  ADD CONSTRAINT `joueur_ibfk_1` FOREIGN KEY (`equipe`) REFERENCES `equipe` (`nom`),
  ADD CONSTRAINT `joueur_ibfk_2` FOREIGN KEY (`jeu`) REFERENCES `jeux` (`jeu`),
  ADD CONSTRAINT `joueur_ibfk_3` FOREIGN KEY (`niveau`) REFERENCES `niveau` (`niveau`);

--
-- Contraintes pour la table `niveau`
--
ALTER TABLE `niveau`
  ADD CONSTRAINT `niveau_ibfk_1` FOREIGN KEY (`jeu`) REFERENCES `jeux` (`jeu`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
